#Clone repositories
git clone https://gitlab.com/simplificamidias/teleinforme/api
git clone https://gitlab.com/simplificamidias/teleinforme/Web

cd ./api/
npm install 
cd ../Web/
npm install 

#MongoDB 5.0
sudo apt-get install gnupg
wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | sudo apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/5.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-5.0.list
sudo apt-get update
sudo apt-get install -y mongodb-org
sudo rm /var/lib/mongodb/mongod.lock
sudo mongod --fork -f /etc/mongod.conf 